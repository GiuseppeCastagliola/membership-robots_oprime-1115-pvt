package com.odigeo.membership.robots.dto;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import java.time.LocalDate;

public class SearchMembershipsDTOTest extends BeanTest<SearchMembershipsDTO> {

    public static final LocalDate NOW = LocalDate.now();
    private static final String ENABLED = "ENABLED";
    private static final String ACTIVATED = "ACTIVATED";

    @Override
    protected SearchMembershipsDTO getBean() {
        return SearchMembershipsDTO.builder()
                .autoRenewal(ENABLED)
                .fromExpirationDate(NOW)
                .toExpirationDate(NOW)
                .status(ACTIVATED)
                .withMemberAccount(false)
                .build();
    }

    @Test
    public void membershipOfferDtoEqualsVerifierTest() {
        EqualsVerifier.forClass(SearchMembershipsDTO.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}