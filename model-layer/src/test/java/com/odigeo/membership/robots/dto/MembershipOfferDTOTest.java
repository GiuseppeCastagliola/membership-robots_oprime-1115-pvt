package com.odigeo.membership.robots.dto;

import bean.test.BeanTest;
import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.testng.annotations.Test;

import java.math.BigDecimal;

public class MembershipOfferDTOTest extends BeanTest<MembershipOfferDTO> {

    public static final String EUR = "EUR";
    private static final String OFFER_ID = "111";
    private static final Long TEST_ID = 1L;
    private static final String WEBSITE = "ES";

    @Override
    protected MembershipOfferDTO getBean() {
        return MembershipOfferDTO.builder()
                .currencyCode(EUR)
                .offerId(OFFER_ID)
                .price(BigDecimal.TEN)
                .productId(TEST_ID)
                .website(WEBSITE)
                .build();
    }

    @Test
    public void membershipOfferDtoEqualsVerifierTest() {
        EqualsVerifier.forClass(MembershipOfferDTO.class)
                .suppress(Warning.STRICT_INHERITANCE, Warning.NONFINAL_FIELDS)
                .usingGetClass()
                .verify();
    }
}