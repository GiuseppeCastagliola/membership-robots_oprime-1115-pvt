package com.odigeo.membership.robots.mapper.response;

import com.odigeo.bookingapi.mock.v9.response.BookingBasicInfoBuilder;
import com.odigeo.bookingapi.mock.v9.response.BookingDetailBuilder;
import com.odigeo.bookingapi.mock.v9.response.BuilderException;
import com.odigeo.bookingapi.v9.responses.BookingDetail;
import com.odigeo.bookingsearchapi.mock.v1.response.BookingSummaryBuilder;
import com.odigeo.bookingsearchapi.v1.responses.BookingSummary;
import com.odigeo.membership.robots.dto.BookingDTO;
import org.mapstruct.factory.Mappers;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class BookingResponseMapperTest {

    private static final BookingResponseMapper BOOKING_RESPONSE_MAPPER = Mappers.getMapper(BookingResponseMapper.class);
    private static final Long BOOKING_ID = 1L;
    private Random random = new Random();
    private BookingSummary bookingSummary;
    private BookingDetail bookingDetail;

    @BeforeMethod
    public void setUp() throws BuilderException, com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        bookingDetail = new BookingDetailBuilder().bookingBasicInfoBuilder(new BookingBasicInfoBuilder().id(BOOKING_ID)).build(random);
        bookingSummary = new BookingSummaryBuilder().membershipId(1L)
                .bookingBasicInfo(new com.odigeo.bookingsearchapi.mock.v1.response.BookingBasicInfoBuilder().id(BOOKING_ID))
                .build(random);
    }

    @Test
    public void testBookingDetailResponseToDto() {
        BookingDTO bookingDTO = BOOKING_RESPONSE_MAPPER.bookingDetailResponseToDto(bookingDetail);
        assertEquals(bookingDTO.getId(), bookingDetail.getBookingBasicInfo().getId());
        assertEquals(bookingDTO.getCurrencyCode(), bookingDetail.getCurrencyCode());
        assertEquals(bookingDTO.getMembershipId(), 0L);
        assertEquals(bookingDTO.getFeeContainers().size(), bookingDetail.getAllFeeContainers().size());
        assertEquals(Long.valueOf(bookingDTO.getUserId()), bookingDetail.getUserId());
    }

    @Test
    public void testBookingDetailResponseNullUserIdToDto() {
        bookingDetail.setUserId(null);
        BookingDTO bookingDTO = BOOKING_RESPONSE_MAPPER.bookingDetailResponseToDto(bookingDetail);
        assertEquals(bookingDTO.getId(), bookingDetail.getBookingBasicInfo().getId());
        assertEquals(bookingDTO.getCurrencyCode(), bookingDetail.getCurrencyCode());
        assertEquals(bookingDTO.getMembershipId(), 0L);
        assertEquals(bookingDTO.getUserId(),0L);
        assertEquals(bookingDTO.getFeeContainers().size(), bookingDetail.getAllFeeContainers().size());
    }

    @Test
    public void testBookingDetailResponseNullToDto() {
        assertNull(BOOKING_RESPONSE_MAPPER.bookingDetailResponseToDto(null));
    }

    @Test
    public void testBookingSummaryResponseToDto() {
        BookingDTO bookingDTO = BOOKING_RESPONSE_MAPPER.bookingSummaryResponseToDto(bookingSummary);
        assertEquals(Long.valueOf(bookingDTO.getMembershipId()), bookingSummary.getMembershipId());
        assertEquals(bookingDTO.getId(), bookingSummary.getBookingBasicInfo().getId());
    }

    @Test
    public void testBookingSummaryResponseNullToDto() {
        assertNull(BOOKING_RESPONSE_MAPPER.bookingSummaryResponseToDto(null));
    }

    @Test
    public void testBookingSummaryResponseNullMembershipIdToDto() throws com.odigeo.bookingsearchapi.mock.v1.response.BuilderException {
        BookingDTO bookingDTO = BOOKING_RESPONSE_MAPPER.bookingSummaryResponseToDto(new BookingSummaryBuilder().membershipId(null).build(new Random()));
        assertNotNull(bookingDTO);
        assertEquals(bookingDTO.getMembershipId(), 0L);
    }

    @Test
    public void testBookingResponseToDTO() {
        BookingDTO bookingDetailDTO = BOOKING_RESPONSE_MAPPER.bookingDetailResponseToDto(bookingDetail);
        BookingDTO bookingSummaryDTO = BOOKING_RESPONSE_MAPPER.bookingSummaryResponseToDto(bookingSummary);
        BookingDTO bookingDTO = BOOKING_RESPONSE_MAPPER.bookingResponsesToDto(bookingSummaryDTO, bookingDetailDTO);
        assertEquals(bookingDTO.getId(), bookingSummaryDTO.getId());
        assertEquals(bookingDTO.getCurrencyCode(), bookingDetailDTO.getCurrencyCode());
        assertEquals(bookingDTO.getMembershipId(), bookingSummaryDTO.getMembershipId());
        assertEquals(bookingDTO.getFeeContainers().size(), bookingDetailDTO.getFeeContainers().size());
    }

    @Test
    public void testBookingResponsesNullToDTO() {
        assertNull(BOOKING_RESPONSE_MAPPER.bookingResponsesToDto(null, null));
    }

    @Test(expectedExceptions = IllegalStateException.class)
    public void testBookingResponseToDTOException() {
        BookingDTO bookingDetailDTO = BOOKING_RESPONSE_MAPPER.bookingDetailResponseToDto(bookingDetail);
        BookingDTO bookingSummaryDTO = BookingDTO.builder().build();
        BOOKING_RESPONSE_MAPPER.bookingResponsesToDto(bookingSummaryDTO, bookingDetailDTO);
    }

    @Test
    public void testBookingSummariesResponsesToDto() {
        List<BookingDTO> bookingDTOS = BOOKING_RESPONSE_MAPPER.bookingSummariesResponseToDto(Arrays.asList(bookingSummary, bookingSummary, bookingSummary));
        assertEquals(bookingDTOS.size(), 3);
    }

    @Test
    public void testBookingSummariesResponseNullToDto() {
        assertNull(BOOKING_RESPONSE_MAPPER.bookingSummariesResponseToDto(null));
    }


}