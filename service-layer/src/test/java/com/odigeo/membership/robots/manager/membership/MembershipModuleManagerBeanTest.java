package com.odigeo.membership.robots.manager.membership;

import com.odigeo.membership.MembershipService;
import com.odigeo.membership.exception.MembershipInternalServerErrorException;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.request.product.creation.CreatePendingToCollectRequest;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;
import com.odigeo.membership.robots.mapper.request.MembershipRequestMapper;
import com.odigeo.membership.robots.mapper.response.MembershipResponseMapper;
import org.mapstruct.factory.Mappers;
import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.MockitoAnnotations.initMocks;
import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertNull;
import static org.testng.Assert.assertTrue;

public class MembershipModuleManagerBeanTest {
    private static final long TEST_MEMBERSHIP_ID = 1L;
    private static final long TEST_MEMBERSHIP_PENDING_TO_COLLECT_ID = 2L;
    private static final String EXPIRED = "EXPIRED";
    private static final String ACTIVATED = "ACTIVATED";
    private static final String TEST_WEBSITE = "ES";
    private static final String NON_RECURRING = "NON_RECURRING";
    private static final String BUSINESS = "BUSINESS";
    private static final String POST_BOOKING = "POST_BOOKING";
    private static final long TEST_MEMBERACCOUNT_ID = 3L;
    private static final String EUR = "EUR";
    public static final LocalDateTime NOW = LocalDateTime.now();
    private static final MembershipDTO MEMBERSHIP_DTO = MembershipDTO.builder().status(ACTIVATED).id(TEST_MEMBERSHIP_ID).balance(BigDecimal.TEN).build();
    private static final MembershipOfferDTO MEMBERSHIP_OFFER_DTO = MembershipOfferDTO.builder().website(TEST_WEBSITE).price(BigDecimal.TEN).currencyCode(EUR).build();

    @Mock
    private MembershipService membershipService;

    private MembershipModuleManagerBean membershipModuleManagerBean;
    private static final MembershipInternalServerErrorException MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION = new MembershipInternalServerErrorException("Exception");
    public static final MembershipDTO.Builder COMMON_BUILDER = MembershipDTO.builder()
            .id(TEST_MEMBERSHIP_ID)
            .website(TEST_WEBSITE)
            .recurringId(NON_RECURRING)
            .membershipType(BUSINESS)
            .sourceType(POST_BOOKING)
            .memberAccountId(TEST_MEMBERACCOUNT_ID)
            .currencyCode(EUR);
    public static final MembershipDTO PREVIOUS_MEMBERSHIP_DTO = COMMON_BUILDER
            .expirationDate(NOW)
            .build();
    public static final MembershipDTO PREVIOUS_MEMBERSHIP_DTO_NULL_EXPIRATION = COMMON_BUILDER
            .expirationDate(null)
            .build();
    private MembershipResponseMapper membershipResponseMapper;

    @BeforeMethod
    public void setup() {
        initMocks(this);
        membershipResponseMapper = Mappers.getMapper(MembershipResponseMapper.class);
        membershipModuleManagerBean = new MembershipModuleManagerBean(membershipService, Mappers.getMapper(MembershipRequestMapper.class), membershipResponseMapper);
    }

    @Test
    public void expireMembershipTest() {
        given(membershipService.updateMembership(any(UpdateMembershipRequest.class))).willReturn(true);
        final ApiCallWrapper<MembershipDTO, MembershipDTO> expireMembershipWrapper = membershipModuleManagerBean.expireMembership(MEMBERSHIP_DTO);
        assertTrue(expireMembershipWrapper.isSuccessful());
        assertEquals(expireMembershipWrapper.getResponse().getStatus(), EXPIRED);
    }

    @Test
    public void expireMembershipFailedTest() {
        given(membershipService.updateMembership(any(UpdateMembershipRequest.class))).willReturn(false);
        final ApiCallWrapper<MembershipDTO, MembershipDTO> expireMembershipWrapper = membershipModuleManagerBean.expireMembership(MEMBERSHIP_DTO);
        assertFalse(expireMembershipWrapper.isSuccessful());
        assertEquals(expireMembershipWrapper.getResponse().getStatus(), ACTIVATED);
    }

    @Test
    public void expireMembershipExceptionTest() {
        given(membershipService.updateMembership(any(UpdateMembershipRequest.class))).willThrow(MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION);
        final ApiCallWrapper<MembershipDTO, MembershipDTO> expireMembershipWrapper = membershipModuleManagerBean.expireMembership(MEMBERSHIP_DTO);
        assertFalse(expireMembershipWrapper.isSuccessful());
        assertEquals(expireMembershipWrapper.getResponse().getStatus(), ACTIVATED);
        assertEquals(expireMembershipWrapper.getException(), MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION);
        assertEquals(expireMembershipWrapper.getMessage(), MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION.getMessage());
    }

    @Test
    public void expireMembershipAlreadyExpiredTest() {
        MembershipDTO expiredDto = membershipResponseMapper.dtoToExpiredDto(MEMBERSHIP_DTO);
        final ApiCallWrapper<MembershipDTO, MembershipDTO> expireMembershipWrapper = membershipModuleManagerBean.expireMembership(expiredDto);
        then(membershipService).should(never()).updateMembership(any());
        assertTrue(expireMembershipWrapper.isSuccessful());
        assertEquals(expireMembershipWrapper.getResponse().getStatus(), EXPIRED);
        assertNull(expireMembershipWrapper.getException());
    }

    @Test
    public void createPendingToCollectTest() {
        given(membershipService.createMembership(any(CreatePendingToCollectRequest.class))).willReturn(TEST_MEMBERSHIP_PENDING_TO_COLLECT_ID);
        final ApiCallWrapper<MembershipDTO, MembershipDTO> pendingToCollectWrapper = membershipModuleManagerBean.createPendingToCollect(PREVIOUS_MEMBERSHIP_DTO, MEMBERSHIP_OFFER_DTO);
        assertTrue(pendingToCollectWrapper.isSuccessful());
        assertEquals(pendingToCollectWrapper.getResponse().getId(), TEST_MEMBERSHIP_PENDING_TO_COLLECT_ID);
        assertEquals(pendingToCollectWrapper.getResponse().getCurrencyCode(), MEMBERSHIP_OFFER_DTO.getCurrencyCode());
        assertEquals(pendingToCollectWrapper.getResponse().getTotalPrice(), MEMBERSHIP_OFFER_DTO.getPrice());
    }

    @Test
    public void createPendingToCollectNullExpirationTest() {
        final ApiCallWrapper<MembershipDTO, MembershipDTO> pendingToCollectWrapper = membershipModuleManagerBean.createPendingToCollect(PREVIOUS_MEMBERSHIP_DTO_NULL_EXPIRATION, MEMBERSHIP_OFFER_DTO);
        assertFalse(pendingToCollectWrapper.isSuccessful());
        assertTrue(pendingToCollectWrapper.getMessage().contains("Expiration date should not be null"));
    }

    @Test
    public void createPendingToCollectExceptionTest() {
        given(membershipService.createMembership(any(CreatePendingToCollectRequest.class))).willThrow(MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION);
        final ApiCallWrapper<MembershipDTO, MembershipDTO> pendingToCollectWrapper = membershipModuleManagerBean.createPendingToCollect(PREVIOUS_MEMBERSHIP_DTO, MEMBERSHIP_OFFER_DTO);
        assertFalse(pendingToCollectWrapper.isSuccessful());
        assertEquals(pendingToCollectWrapper.getResponse().getId(), PREVIOUS_MEMBERSHIP_DTO.getId());
        assertEquals(pendingToCollectWrapper.getException(), MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION);
        assertEquals(pendingToCollectWrapper.getMessage(), MEMBERSHIP_INTERNAL_SERVER_ERROR_EXCEPTION.getMessage());
    }

    @Test
    public void testConsumeRemnantMembershipBalance() {
        given(membershipService.updateMembership(any(UpdateMembershipRequest.class))).willReturn(true);
        final ApiCallWrapper<MembershipDTO, MembershipDTO> consumeRemnantBalanceWrapper = membershipModuleManagerBean.consumeRemnantMembershipBalance(MEMBERSHIP_DTO);
        assertTrue(consumeRemnantBalanceWrapper.isSuccessful());
        assertEquals(consumeRemnantBalanceWrapper.getResponse().getBalance(), BigDecimal.ZERO);
    }
}