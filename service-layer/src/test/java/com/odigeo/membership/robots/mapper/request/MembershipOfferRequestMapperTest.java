package com.odigeo.membership.robots.mapper.request;

import com.odigeo.membership.offer.api.request.SearchCriteriaRequest;
import com.odigeo.membership.robots.dto.MembershipDTO;
import org.mapstruct.factory.Mappers;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class MembershipOfferRequestMapperTest {

    private static final String WEBSITE = "ES";
    private static final long TEST_ID = 1L;
    private static final String SIX_M_AB = "X20-4";
    private static final MembershipOfferRequestMapper MAPPER = Mappers.getMapper(MembershipOfferRequestMapper.class);

    @Test
    public void testDtoToSearchCriteriaRequest() {
        final SearchCriteriaRequest searchCriteriaRequest = MAPPER.dtoToSearchCriteriaRequest(MembershipDTO.builder().id(TEST_ID).website(WEBSITE).build());
        assertNotNull(searchCriteriaRequest);
        assertEquals(searchCriteriaRequest.getSearchCriteria().getWebsiteCode(), WEBSITE);
        assertEquals(searchCriteriaRequest.getSearchCriteria().getMembershipId().longValue(), TEST_ID);
        assertEquals(searchCriteriaRequest.getSearchCriteria().getTestTokenSet().getDimensionPartitionNumberMap().get(SIX_M_AB).intValue(), 1);
    }

    @Test
    public void testDtoNullToSearchCriteriaRequest() {
        assertNull(MAPPER.dtoToSearchCriteriaRequest(null));
    }

}