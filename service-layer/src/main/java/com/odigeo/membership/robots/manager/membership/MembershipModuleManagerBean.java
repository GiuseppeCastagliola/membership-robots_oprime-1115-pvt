package com.odigeo.membership.robots.manager.membership;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import com.odigeo.membership.MembershipService;
import com.odigeo.membership.exception.MembershipServiceException;
import com.odigeo.membership.request.product.UpdateMembershipRequest;
import com.odigeo.membership.request.product.creation.CreatePendingToCollectRequest;
import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.apicall.ApiCallWrapperBuilder;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;
import com.odigeo.membership.robots.mapper.request.MembershipRequestMapper;
import com.odigeo.membership.robots.mapper.response.MembershipResponseMapper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.UndeclaredThrowableException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Optional;
import java.util.StringJoiner;
import java.util.function.Function;

import static com.odigeo.membership.robots.apicall.ApiCall.Endpoint;
import static com.odigeo.membership.robots.apicall.ApiCall.Result;

@Singleton
public class MembershipModuleManagerBean implements MembershipModuleManager {
    private static final String EXPIRED_MSG = " expired correctly";
    private static final String CONSUMED_MSG = " balance consumed correctly";
    private static final String ALREADY_EXPIRED_MSG = " was already expired correctly";
    private static final Logger LOGGER = LoggerFactory.getLogger(MembershipModuleManagerBean.class);
    private static final String COLON = " : ";
    private static final Function<LocalDateTime, String> NEXT_EXPIRATION_DATE = previousExpirationDate -> DateTimeFormatter.ISO_DATE.format(previousExpirationDate.plusYears(1L));

    private final MembershipService membershipService;
    private final MembershipRequestMapper membershipRequestMapper;
    private final MembershipResponseMapper membershipResponseMapper;

    @Inject
    public MembershipModuleManagerBean(MembershipService membershipService, MembershipRequestMapper membershipRequestMapper, MembershipResponseMapper membershipResponseMapper) {
        this.membershipService = membershipService;
        this.membershipRequestMapper = membershipRequestMapper;
        this.membershipResponseMapper = membershipResponseMapper;
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> expireMembership(MembershipDTO membershipToExpire) {
        if ("EXPIRED".equalsIgnoreCase(membershipToExpire.getStatus())) {
            ApiCallWrapper<MembershipDTO, MembershipDTO> expireWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.EXPIRE_MEMBERSHIP)
                    .result(Result.SUCCESS)
                    .response(membershipToExpire)
                    .message(membershipToExpire.getId() + ALREADY_EXPIRED_MSG)
                    .request(membershipToExpire).build();
            expireWrapper.logResultAndMessageWithPrefix(LOGGER, expireWrapper.getEndpoint() + StringUtils.SPACE + membershipToExpire.getId() + COLON);
            return expireWrapper;
        }
        final UpdateMembershipRequest expireMembershipRequest = membershipRequestMapper.dtoToExpireRequest(membershipToExpire);
        final ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> expireWrapperBuilder = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.EXPIRE_MEMBERSHIP)
                .request(membershipToExpire);
        return updateMembership(membershipToExpire, expireMembershipRequest, expireWrapperBuilder, EXPIRED_MSG);
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> createPendingToCollect(MembershipDTO previousMembership, MembershipOfferDTO membershipOfferDTO) {
        ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> createPendingToCollectWrapperBuilder = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.CREATE_PENDING_TO_COLLECT)
                .result(Result.FAIL).request(previousMembership);
        Optional<String> newExpirationDate = Optional.ofNullable(previousMembership.getExpirationDate()).map(NEXT_EXPIRATION_DATE);
        if (newExpirationDate.isPresent()) {
            final CreatePendingToCollectRequest pendingToCollectRequest = membershipRequestMapper
                    .dtoToCreatePendingToCollectRequestBuilder(previousMembership, membershipOfferDTO, newExpirationDate.get())
                    .build();
            wrapPendingToCollectCreation(previousMembership, createPendingToCollectWrapperBuilder, pendingToCollectRequest);
        } else {
            createPendingToCollectWrapperBuilder.response(previousMembership).message("Expiration date should not be null!");
        }
        return logResult(previousMembership, createPendingToCollectWrapperBuilder.build());
    }

    @Override
    public ApiCallWrapper<MembershipDTO, MembershipDTO> consumeRemnantMembershipBalance(MembershipDTO membershipToConsume) {
        final UpdateMembershipRequest consumeBalanceRequest = membershipRequestMapper.dtoToResetBalanceRequest(membershipToConsume);
        final ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> consumeBalanceWrapper = new ApiCallWrapperBuilder<MembershipDTO, MembershipDTO>(Endpoint.CONSUME_MEMBERSHIP_REMNANT_BALANCE)
                .request(membershipToConsume);
        return updateMembership(membershipToConsume, consumeBalanceRequest, consumeBalanceWrapper, CONSUMED_MSG);
    }

    private ApiCallWrapper<MembershipDTO, MembershipDTO> updateMembership(MembershipDTO membershipToUpdate, UpdateMembershipRequest updateMembershipRequest, ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> updateWrapper, String successMsg) {
        try {
            if (membershipService.updateMembership(updateMembershipRequest)) {
                updateWrapper.result(Result.SUCCESS)
                        .response("EXPIRE_MEMBERSHIP".equalsIgnoreCase(updateMembershipRequest.getOperation())
                                ? membershipResponseMapper.dtoToExpiredDto(membershipToUpdate)
                                : membershipResponseMapper.dtoToConsumedBalanceDto(membershipToUpdate))
                        .message(membershipToUpdate.getId() + successMsg);
            } else {
                updateWrapper.result(Result.FAIL).response(membershipToUpdate);
            }
        } catch (MembershipServiceException | UndeclaredThrowableException e) {
            LOGGER.error(e.getMessage(), e);
            updateWrapper.result(Result.FAIL).response(membershipToUpdate).exception(e).message(e.getMessage());
        }
        final ApiCallWrapper<MembershipDTO, MembershipDTO> result = updateWrapper.build();
        result.logResultAndMessageWithPrefix(LOGGER, result.getEndpoint() + StringUtils.SPACE + membershipToUpdate.getId() + COLON);
        return result;
    }

    private void wrapPendingToCollectCreation(MembershipDTO previousMembership, ApiCallWrapperBuilder<MembershipDTO, MembershipDTO> createPendingToCollectWrapperBuilder, CreatePendingToCollectRequest pendingToCollectRequest) {
        try {
            final Long membershipId = membershipService.createMembership(pendingToCollectRequest);
            final MembershipDTO enrichedMembershipResponse = fillResponseMembershipDTO(previousMembership, pendingToCollectRequest, membershipId);
            createPendingToCollectWrapperBuilder.result(Result.SUCCESS).response(enrichedMembershipResponse).message("Membership PENDING_TO_COLLECT " + membershipId + " created");
        } catch (MembershipServiceException | UndeclaredThrowableException e) {
            LOGGER.error(e.getMessage(), e);
            createPendingToCollectWrapperBuilder.response(previousMembership).exception(e).message(e.getMessage());
        }
    }

    private MembershipDTO fillResponseMembershipDTO(MembershipDTO previousMembership, CreatePendingToCollectRequest pendingToCollectRequest, Long membershipId) {
        return MembershipDTO.builderFromDto(previousMembership)
                .expirationDate(LocalDate.parse(pendingToCollectRequest.getExpirationDate()).atStartOfDay())
                .totalPrice(pendingToCollectRequest.getSubscriptionPrice())
                .currencyCode(pendingToCollectRequest.getCurrencyCode())
                .monthsDuration(pendingToCollectRequest.getMonthsToRenewal())
                .id(membershipId).build();
    }

    private ApiCallWrapper<MembershipDTO, MembershipDTO> logResult(MembershipDTO membershipDto, ApiCallWrapper<MembershipDTO, MembershipDTO> apiCallWrapper) {
        apiCallWrapper.logResultAndMessageWithPrefix(LOGGER, new StringJoiner(COLON).add(apiCallWrapper.getEndpoint().name())
                .add(" previous membership ")
                .add(String.valueOf(membershipDto.getId()))
                .add(StringUtils.SPACE)
                .toString());
        return apiCallWrapper;
    }
}
