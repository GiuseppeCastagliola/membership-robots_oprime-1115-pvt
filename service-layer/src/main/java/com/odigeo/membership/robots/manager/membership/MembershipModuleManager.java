package com.odigeo.membership.robots.manager.membership;

import com.odigeo.membership.robots.apicall.ApiCallWrapper;
import com.odigeo.membership.robots.dto.MembershipDTO;
import com.odigeo.membership.robots.dto.MembershipOfferDTO;

public interface MembershipModuleManager {
    ApiCallWrapper<MembershipDTO, MembershipDTO> expireMembership(MembershipDTO membershipDTO);

    ApiCallWrapper<MembershipDTO, MembershipDTO> createPendingToCollect(MembershipDTO previousMembership, MembershipOfferDTO membershipOfferDTO);

    ApiCallWrapper<MembershipDTO, MembershipDTO> consumeRemnantMembershipBalance(MembershipDTO membershipToConsume);
}
