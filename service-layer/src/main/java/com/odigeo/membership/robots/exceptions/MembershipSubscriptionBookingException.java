package com.odigeo.membership.robots.exceptions;

public class MembershipSubscriptionBookingException extends Exception {

    public MembershipSubscriptionBookingException(String message) {
        super(message);
    }

    public MembershipSubscriptionBookingException(String message, Throwable e) {
        super(message, e);
    }

    public MembershipSubscriptionBookingException(Throwable e) {
        super(e);
    }
}
